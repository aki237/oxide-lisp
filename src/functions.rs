use crate::oxl::value::Types;
use crate::oxl::value::Value;

pub fn plus(x: Vec<Types>) -> Option<Types> {
    let mut sum = 0.0f64;
    let mut ret_float = false;
    for i in x {
        match i {
            Types::Int(x) => {
                sum += x as f64;
            }
            Types::Float(x) => {
                ret_float = true;
                sum += x;
            }
            _ => {
                return None;
            }
        }
    }
    if ret_float {
        return Some(Types::Float(sum));
    }
    Some(Types::Int(sum as i64))
}

pub fn prod(x: Vec<Types>) -> Option<Types> {
    let mut prod = 1.0f64;
    let mut ret_float = false;
    for i in x {
        match i {
            Types::Int(x) => {
                prod *= x as f64;
            }
            Types::Float(x) => {
                ret_float = true;
                prod *= x;
            }
            _ => {
                return None;
            }
        }
    }
    if ret_float {
        return Some(Types::Float(prod));
    }
    Some(Types::Int(prod as i64))
}

pub fn println(x: Vec<Types>) -> Option<Types> {
    for (i, v) in x.iter().enumerate() {
        print!("{}", v);
        if i < x.len() - 1 {
            print!(" ")
        }
    }
    println!("");
    None
}

pub fn gt(x: Vec<Types>) -> Option<Types> {
    if x.len() != 2 {
        // TODO: send an error context to reply the error back.
        panic!("wrong args");
    }
    let mut ic = x.into_iter();
    match (ic.next(), ic.next()) {
        (Some(Types::Int(a)), Some(Types::Int(b))) => {
            return Some(Types::Bool(a > b));
        }
        (Some(Types::Float(a)), Some(Types::Float(b))) => {
            return Some(Types::Bool(a > b));
        }
        (Some(Types::Int(a)), Some(Types::Float(b))) => {
            return Some(Types::Bool(a as f64 > b));
        }
        (Some(Types::Float(a)), Some(Types::Int(b))) => {
            return Some(Types::Bool(a > b as f64));
        }
        (arg1, arg2) => panic!("Unsupported types for comparison: {:?}, {:?}", arg1, arg2),
    }
}

pub fn modulo(x: Vec<Types>) -> Option<Types> {
    if x.len() != 2 {
        panic!("wrong args");
    }
    let mut ic = x.into_iter();
    let num1 = ic.next().unwrap();
    let num2 = ic.next().unwrap();
    match (num1, num2) {
        (Types::Int(x), Types::Int(y)) => Some(Types::Int(x % y)),
        (Types::Float(x), Types::Float(y)) => Some(Types::Float(x % y)),
        (Types::Int(x), Types::Float(y)) => Some(Types::Float((x as f64) % y)),
        (Types::Float(x), Types::Int(y)) => Some(Types::Float(x as f64 % (y as f64))),
        (arg1, arg2) => panic!("Unsupported types for modulo: {:?}, {:?}", arg1, arg2),
    }
}

pub fn eq(x: Vec<Types>) -> Option<Types> {
    if x.len() != 2 {
        // TODO: send an error context to reply the error back.
        panic!("wrong args");
    }
    let mut ic = x.into_iter();
    Some(Types::Bool(ic.next().unwrap() == ic.next().unwrap()))
}

pub fn and(x: Vec<Types>) -> Option<Types> {
    for i in x {
        match i.as_bool() {
            Some(true) => {
                continue;
            }
            Some(false) => {
                return Some(Types::Bool(false));
            }
            None => {
                panic!("and: cannot be casted to bool")
            }
        }
    }

    Some(Types::Bool(true))
}

pub fn or(x: Vec<Types>) -> Option<Types> {
    for i in x {
        match i.as_bool() {
            Some(true) => {
                return Some(Types::Bool(true));
            }
            Some(false) => {
                continue;
            }
            None => {
                panic!("or: cannot be casted to bool")
            }
        }
    }

    Some(Types::Bool(false))
}

pub fn not(x: Vec<Types>) -> Option<Types> {
    if x.len() != 1 {
        panic!("wrong args");
    }

    Some(
        Types::Bool(
            !x.into_iter().next().unwrap().as_bool().unwrap()
        )
    )
}