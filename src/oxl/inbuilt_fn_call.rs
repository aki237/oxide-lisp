use crate::oxl::ast::*;
use crate::oxl::vm::*;
use crate::oxl::value::*;
use crate::oxl::token::*;

#[derive(Debug)]
pub struct Call {
    func: String,
    args: Vec<Evalable>,
    // File Bounds
}

impl Call {
    pub fn eval(&self, scope: &mut dyn Scope) -> Result<Types, String> {
        if !scope.has_func(String::from(&self.func)) {
            return Err(String::from(format!(
                "function '{}' not found in scope",
                self.func
            )));
        }

        let mut final_args: Vec<Types> = vec![];
        for i in self.args.iter() {
            match i.eval(scope) {
                Ok(a) => {
                    final_args.push(a);
                }
                Err(e) => return Err(String::from(e)),
            }
        }

        Ok(scope.get_func(String::from(&self.func)).unwrap()(final_args).unwrap_or(Types::None))
    }

    pub fn from(call: Token, func: String, args: Vec<ArgumentTypes>) -> Evalable {
        Evalable::SUBEXP(SubExpression::FnCall(Call::new(
            func,
            Evalable::argtvec_to_evalables(args, call.line, call.col),
        )))
    }

    pub fn new(func: String, args: Vec<Evalable>) -> Call {
        Call {
            func: func,
            args: args,
        }
    }
}