use crate::oxl::ast::*;
use crate::oxl::vm::*;
use crate::oxl::value::*;
use crate::oxl::token::*;

#[derive(Debug)]
pub struct SwitchBlock {
    expr: HeapEval,
    cases: Vec<(HeapEval, HeapEval)>,
    default: HeapEval,
    // File Bounds
}

impl SwitchBlock {
    pub fn eval(&self, scope: &mut dyn Scope) -> Result<Types, String> {
        let result = self.expr.eval(scope);
        if result.is_err() {
            return result;
        }
        let match_with = result.unwrap();

        for (condition, body) in &self.cases {
            match condition.eval(scope) {
                Ok(case_eval) => {
                    if case_eval != match_with {
                        continue;
                    }
                    return body.eval(scope);
                }
                Err(err) => {
                    return Err(err);
                }
            }
        }

        self.default.eval(scope)
    }

    pub fn from(call: Token, args: Vec<ArgumentTypes>) -> Evalable {
        if args.len() < 2 || args.len() % 2 != 1 {
            panic!(
                "switch requires condition with match arms with body as well, got only {:?} args {}:{}",
                args.len(),
                call.line,
                call.col,
            )
        }
        let mut args_iter = args.into_iter();
        let expr = {
            let cond = args_iter.next().unwrap();
            let err = format!(
                "expected a simple switch expression, got {:?} {}:{}",
                cond, call.line, call.col,
            );
            cond.as_evalable().expect(err.as_str())
        };
        let mut arms = Vec::new();
        let mut default = Evalable::NONE;
        loop {
            let val = args_iter.next();
            if val.is_none() {
                break;
            }
            match (val.unwrap(), args_iter.next().unwrap()) {
                (ArgumentTypes::SwitchDefault, ArgumentTypes::Evalable(def)) => {
                    default = def;
                }
                (ArgumentTypes::Evalable(case), ArgumentTypes::Evalable(body)) => {
                    arms.push((Box::new(case), Box::new(body)));
                }
                (case, body) => panic!(
                    "expected a case and a condition, got {:?} and {:?}, {}:{}",
                    case, body, call.line, call.col,
                ),
            }
        }

        Evalable::SUBEXP(SubExpression::Switch(SwitchBlock::new(
            Box::new(expr),
            arms,
            Box::new(default),
        )))
    }

    pub fn new(expr: HeapEval, cases: Vec<(HeapEval, HeapEval)>, default: HeapEval) -> SwitchBlock {
        SwitchBlock {
            expr,
            cases,
            default,
        }
    }
}
