use std::fmt;

pub trait Value: PartialEq {
    fn as_int(&self) -> Option<i64>;
    fn as_float(&self) -> Option<f64>;
    fn as_string(&self) -> Option<String>;
    fn as_bool(&self) -> Option<bool>;

    fn is_custom(&self) -> bool;
}

#[derive(Debug)]
pub enum Types {
    Int(i64),
    Float(f64),
    Bool(bool),
    Str(String),
    None,
}

impl Types {
    pub fn clone(&self) -> Types {
        match self {
            Types::Int(x) => Types::Int(*x),
            Types::Float(x) => Types::Float(*x),
            Types::Bool(x) => Types::Bool(*x),
            Types::Str(x) => Types::Str(String::from(x)),
            _ => Types::None,
        }
    }
}

impl Value for Types {
    fn as_int(&self) -> Option<i64> {
        match self {
            Types::Int(x) => Some(*x),
            Types::Float(x) => Some(*x as i64),
            Types::Bool(_x) => None,
            Types::Str(x) => x.parse::<i64>().ok(),
            _ => None,
        }
    }

    fn as_float(&self) -> Option<f64> {
        match self {
            Types::Int(x) => Some(*x as f64),
            Types::Float(x) => Some(*x),
            Types::Bool(_x) => None,
            Types::Str(x) => x.parse::<f64>().ok(),
            _ => None,
        }
    }

    fn as_string(&self) -> Option<String> {
        match self {
            Types::Int(x) => Some(x.to_string()),
            Types::Float(x) => Some(x.to_string()),
            Types::Bool(x) => Some(x.to_string()),
            Types::Str(x) => Some(String::from(x)),
            _ => Some(String::from("none")),
        }
    }

    fn as_bool(&self) -> Option<bool> {
        match self {
            Types::Int(x) => {
                if *x <= 0 {
                    return Some(false);
                }
                return Some(true);
            }
            Types::Float(x) => {
                if *x <= 0.0 {
                    return Some(false);
                }
                return Some(true);
            }
            Types::Bool(x) => Some(*x),
            Types::Str(x) => {
                if x.len() > 0 {
                    return Some(true);
                }

                Some(false)
            }
            _ => Some(false),
        }
    }

    fn is_custom(&self) -> bool {
        false
    }
}

macro_rules! same {
    ($x:expr, $y:expr) => {
        return $x == $y;
    };
}

impl PartialEq for Types {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Types::Float(x), Types::Int(y)) => same!(*x, *y as f64),
            (Types::Int(x), Types::Float(y)) => same!(*x as f64, *y),
            (Types::Float(x), Types::Float(y)) => same!(*x, *y),
            (Types::Int(x), Types::Int(y)) => same!(*x, *y),
            (Types::Bool(x), Types::Bool(y)) => same!(*x, *y),
            (Types::Str(x), Types::Str(y)) => same!(*x, *y),
            (Types::None, Types::None) => {
                return true;
            }
            _ => false,
        }
    }
}

impl fmt::Display for Types {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            Types::Int(x) => write!(f, "{}", x),
            Types::Float(x) => write!(f, "{}", x),
            Types::Bool(x) => write!(f, "{}", x),
            Types::Str(x) => write!(f, "{}", x),
            Types::None => write!(f, "nil"),
        }
    }
}