use lazy_static;
use regex::Regex;
use std::fmt;
use crate::oxl::value::*;

#[derive(Clone)]
pub struct Token {
    pub token_type: TokenType,
    pub position: u64,
    pub line: u64,
    pub col: u64,
}

impl fmt::Display for Token {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "Token{{@{}:{}, offset:{}, {}}}", self.line, self.col, self.position, self.token_type)
    }
}

#[derive(Clone)]
pub enum TokenType {
    COMMENT(String),
    LPAREN,
    RPAREN,
    KEYWORD(Keywords),
    IDENTIFIER(String),
    LITERAL(LiteralType),
    ATOM(String),
    UNINITIALIZED,
}

impl fmt::Display for TokenType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            TokenType::COMMENT(x) => write!(f, "COMMENT<{}>", x),
            TokenType::LPAREN => write!(f, "LPAREN<(>"),
            TokenType::RPAREN => write!(f, "RPAREN<)>"),
            TokenType::KEYWORD(x) => write!(f, "KEYWORD<{}>", x),
            TokenType::IDENTIFIER(id) => write!(f, "IDENTIFIER<{}>", id),
            TokenType::LITERAL(id) => write!(f, "LITERAL<{}>", id),
            TokenType::ATOM(atom) => write!(f, "ATOM<{}>", atom),
            // Internal use only
            TokenType::UNINITIALIZED => write!(f, "UNINIT")
        }
    }
}

const SEMI_COLON :u8 = ';' as u8;
const COLON :u8 = ':' as u8;

impl TokenType {
    pub fn get_token_type(s: &String) -> TokenType {
        match s.as_str() {
            "(" => TokenType::LPAREN,
            ")" => TokenType::RPAREN,
            x => {
                match x.bytes().nth(0) {
                    Some(SEMI_COLON) => {return TokenType::COMMENT(String::from(x));},
                    Some(COLON) => {return TokenType::ATOM(String::from(x));},
                    Some(_) => {},
                    None => {}
                }
                let result = Keywords::from(String::from(x));
                if result.is_ok() {
                    return TokenType::KEYWORD(result.unwrap());
                }
                match LiteralType::from(String::from(x)) {
                    Ok(x) => TokenType::LITERAL(x),
                    Err(_) => TokenType::IDENTIFIER(String::from(x))
                }
            }
        }
    }
}

#[derive(Clone)]
pub enum LiteralType {
    INTEGER(i64),
    FLOAT(f64),
    BOOL(bool),
    STRING(String),
}

impl fmt::Display for LiteralType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            LiteralType::INTEGER(i) => write!(f, "INT<{}>", i),
            LiteralType::FLOAT(i) => write!(f, "FLOAT<{}>", i),
            LiteralType::BOOL(i) => write!(f, "BOOL<{}>", i),
            LiteralType::STRING(i) => write!(f, "STRING<{}>", i),
        }
    }
}

lazy_static! {
    pub static ref RHEX_INT: Regex = Regex::new(r"^[-]?0x[a-fA-F0-9]+$").unwrap();
    pub static ref RINT: Regex = Regex::new(r"^[-]?\d+$").unwrap();
    pub static ref RFLOAT: Regex = Regex::new(r"^[-]?\d+\.\d+$").unwrap();
}

impl LiteralType {
    pub fn from(tk: String) -> Result<LiteralType, String> {
        if RINT.is_match(tk.as_str()) {
            return Ok(LiteralType::INTEGER(
                tk.as_str().parse::<i64>().unwrap()
            ));
        }

        if RHEX_INT.is_match(tk.as_str()) {
            return Ok(LiteralType::INTEGER(
                LiteralType::hex_to_int(tk.as_str()).unwrap(),
            ));
        }

        if RFLOAT.is_match(tk.as_str()) {
            return Ok(LiteralType::FLOAT(
                tk.as_str().parse::<f64>().unwrap()
            ));
        }

        if tk == "true" || tk == "false" {
            return Ok(LiteralType::BOOL(tk == "true"));
        }

        let mut chars = tk.as_str().chars();
        if chars.nth(0).unwrap() == '"' && chars.last().unwrap() == '"' {
            let mut n = String::from(tk.as_str());
            let _ = n.split_off(n.len() - 1);
            return Ok(LiteralType::STRING(n.split_off(1)));
        }

        Err(format!("'{}' is not a literal", tk))
    }

    fn hex_to_int(strx: &str) -> Option<i64> {
        let mut i = strx.len() - 1;
        let len = i;
        let mut sum: i64 = 0;
        let last_index = if strx.chars().nth(0).unwrap() == '-' {
            2
        } else {
            1
        };
        loop {
            if i <= last_index {
                break;
            }
            let ch = strx.chars().nth(i).unwrap();
            sum += (LiteralType::get_hex_num(ch as u8).unwrap() as i64)
                * (16 as i64).pow((len - i) as u32);
            i = i - 1;
        }
        if last_index == 2 {
            sum *= -1
        }
        Some(sum)
    }

    fn get_hex_num(ascii: u8) -> Option<u8> {
        // a-f
        if ascii >= 97 && ascii <= 102 {
            return Some(ascii - 87);
        }
        // A-F
        if ascii >= 65 && ascii <= 70 {
            return Some(ascii - 55);
        }
        // 0-9
        if ascii >= 48 && ascii <= 57 {
            return Some(ascii - 48);
        }
        None
    }

    pub fn to_type(&self) -> Types {
        match self {
            LiteralType::INTEGER(i) => Types::Int(*i),
            LiteralType::FLOAT(i) => Types::Float(*i),
            LiteralType::BOOL(i) => Types::Bool(*i),
            LiteralType::STRING(i) => Types::Str(String::from(i)),
        }
    }
}

#[derive(Debug, Clone, Copy)]
pub enum Keywords {
    IF,
    ELSE,
    SWITCH,
    CASE,
    DEFAULT,
    WHILE,
    BREAK,
    CONTINUE,
    FN,
    RETURN,
    DEFINE,
    LET,
    SET,
    EVAL,
}

impl Keywords {
    pub fn from(x: String) -> Result<Keywords, String> {
        match x.as_ref() {
            "if" => Ok(Keywords::IF),
            "else" => Ok(Keywords::ELSE),
            "switch" => Ok(Keywords::SWITCH),
            "case" => Ok(Keywords::CASE),
            "default" => Ok(Keywords::DEFAULT),
            "while" => Ok(Keywords::WHILE),
            "break" => Ok(Keywords::BREAK),
            "continue" => Ok(Keywords::CONTINUE),
            "defun" => Ok(Keywords::FN),
            "return" => Ok(Keywords::RETURN),
            "define" => Ok(Keywords::DEFINE),
            "let" => Ok(Keywords::LET),
            "set" => Ok(Keywords::SET),
            "eval" => Ok(Keywords::EVAL),
            _ => {
                let err = format!("invalid keyword '{}'", x);
                Err(err)
            }
        }
    }

    pub fn as_str(&self) -> &'static str {
        match self {
            Keywords::IF => "if",
            Keywords::ELSE => "else",
            Keywords::SWITCH => "switch",
            Keywords::CASE => "case",
            Keywords::DEFAULT => "default",
            Keywords::WHILE => "while",
            Keywords::BREAK => "break",
            Keywords::CONTINUE => "continue",
            Keywords::FN => "defun",
            Keywords::RETURN => "return",
            Keywords::DEFINE => "define",
            Keywords::LET => "let",
            Keywords::SET => "set",
            Keywords::EVAL => "eval",
        }
    }
}

impl fmt::Display for Keywords {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", self.as_str())
    }
}
