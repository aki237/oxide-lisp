//pub struct VM;
use crate::oxl::ast::*;
use crate::oxl::scanner::*;
use crate::oxl::token::*;
use crate::oxl::value::*;
use std::collections::BTreeMap;
use std::collections::HashMap;

pub trait Scope {
    fn is_global_scope(&self) -> bool;
    fn get_variable(&self, name: String) -> Option<Types>;
    fn set_variable(&mut self, name: String, v: Types) -> Result<bool, String>;
    fn get_func(&self, name: String) -> Option<&Function>;
    fn has_func(&self, name: String) -> bool;
    fn put_func(&mut self, name: &str, func: Function);
}

//pub struct GlobalScope;

pub type Function = fn(Vec<Types>) -> Option<Types>;

pub struct GlobalScope {
    vars: HashMap<String, Types>,
    funcs: HashMap<String, Function>,
    tokens: Vec<Token>,
}

#[derive(Debug)]
pub enum ArgumentTypes {
    Evalable(Evalable),
    SwitchDefault,
}

impl ArgumentTypes {
    pub fn as_evalable(self) -> Option<Evalable> {
        match self {
            ArgumentTypes::Evalable(x) => Some(x),
            _ => None,
        }
    }
}

macro_rules! evalarg {
    ($x:expr) => {
        ArgumentTypes::Evalable($x)
    };
}

impl GlobalScope {
    pub fn new() -> GlobalScope {
        GlobalScope {
            vars: HashMap::new(),
            funcs: HashMap::new(),
            tokens: vec![],
        }
    }

    pub fn eval(&mut self, filename: &str) {
        let exps = self.form_evalables_for_file(filename);
        let mut res = Ok(Types::None);
        for i in exps {
            res = i.eval(self);
            if res.is_err() {
                panic!("Error returned: {:?}", res.err().unwrap());
            }
        }

        println!("{:?}", res);
    }

    pub fn form_evalables_for_file(&mut self, filename: &str) -> Vec<Evalable> {
        let x = Scanner::init(String::from(filename));
        self.tokens = x.get_tokens().unwrap();
        let mut start_index = 0usize;

        for (i, tkn) in self.tokens.iter().enumerate() {
            match tkn.token_type {
                TokenType::COMMENT(_) => continue,
                TokenType::LPAREN => {
                    start_index = i;
                    break;
                }
                _ => panic!("Expecting {}, got {}", TokenType::LPAREN, tkn.token_type),
            }
        }

        let top_level = self.get_top_level_offsets(start_index, self.tokens.len());
        let mut evalables: Vec<Evalable> = Vec::with_capacity(top_level.len());
        //println!("Top Levels: {:?}", top_level);

        for (start, end) in top_level.iter() {
            evalables.push(self.form_ast(*start, *end + 1, 0));
        }

        evalables
    }

    fn form_ast(&self, start: usize, end: usize, n: usize) -> Evalable {
        if start >= end || start + 1 == end - 1 {
            return Evalable::NONE;
        }

        let subexp_top_levels = self.get_top_level_offsets(start + 1, end - 1);
        let mut i = start;

        let mut call_name: Token = Token {
            token_type: TokenType::UNINITIALIZED,
            position: 0,
            line: 0,
            col: 0,
        };

        let mut args: Vec<ArgumentTypes> = Vec::new();

        //let prefix = "....".repeat(n);

        while i < end - 1 {
            i += 1;
            let x = &self.tokens[i];
            //println!("{}Traversing: {}", prefix, x.token_type);

            if i == start + 1 {
                match x.token_type {
                    TokenType::KEYWORD(_) | TokenType::IDENTIFIER(_) => {
                        call_name = x.clone();
                    }
                    _ => panic!(
                        "only keywords(IF) and identifiers are allowed {}:{}",
                        x.line, x.col
                    ),
                }
                continue;
            }

            // Check if the current token index is in the top level
            // paren offsets for this specific subexp
            let end_subexp = subexp_top_levels.get(&i);
            if end_subexp.is_some() {
                let end_offset = end_subexp.unwrap();
                args.push(evalarg!(self.form_ast(i, *end_offset + 1, n + 1,)));
                i = *end_offset;
            }

            match &x.token_type {
                TokenType::COMMENT(_) => continue,
                TokenType::LITERAL(x) => args.push(evalarg!(Evalable::LITERAL(x.to_type()))),
                TokenType::IDENTIFIER(var) => {
                    args.push(evalarg!(Evalable::VARIABLE(String::from(var))))
                }
                TokenType::KEYWORD(Keywords::DEFAULT) => match call_name.token_type {
                    TokenType::KEYWORD(Keywords::SWITCH) => {
                        args.push(ArgumentTypes::SwitchDefault);
                    }
                    _ => panic!("got 'default' outside switch block {}:{}", x.line, x.col),
                },
                TokenType::KEYWORD(i) => panic!(
                    "expecting subexpression, identifier or a literal, found keyword '{}' {}:{}",
                    i, x.line, x.col
                ),
                _ => continue,
            }
        }

        //println!("{}calling {} with {:?}", prefix, call_name, args);

        Evalable::from(call_name, args)
    }

    fn get_top_level_offsets(&self, start: usize, end: usize) -> BTreeMap<usize, usize> {
        let mut stack: i64 = 0;
        let mut last_open_offset: usize = 0;
        let mut offsets: BTreeMap<usize, usize> = BTreeMap::new();
        for i in start..end {
            if i >= end {
                break;
            }
            let x = &self.tokens[i];
            match x.token_type {
                TokenType::LPAREN => {
                    if stack == 0 {
                        last_open_offset = i;
                    }
                    stack += 1
                }
                TokenType::RPAREN => {
                    stack -= 1;
                    if stack < 0 {
                        panic!("unmatched closing parenthesis at {}:{}", x.line, x.col)
                    }
                    if stack == 0 {
                        offsets.insert(last_open_offset, i);
                    }
                }
                _ => continue,
            }
        }

        if stack > 0 {
            panic!(
                "unmatched opening parenthesis at {}:{}",
                self.tokens[last_open_offset].line, self.tokens[last_open_offset].col,
            )
        }

        offsets
    }
}

impl Scope for GlobalScope {
    fn is_global_scope(&self) -> bool {
        true
    }

    fn get_variable(&self, name: String) -> Option<Types> {
        let ret = self.vars.get(&name);
        if ret.is_some() {
            return Some(ret.unwrap().clone());
        }
        None
    }

    fn set_variable(&mut self, name: String, v: Types) -> Result<bool, String> {
        self.vars.insert(name, v);
        Ok(true)
    }

    fn get_func(&self, name: String) -> Option<&Function> {
        self.funcs.get(&name)
    }

    fn has_func(&self, name: String) -> bool {
        self.funcs.contains_key(&name)
    }

    fn put_func(&mut self, name: &str, func: Function) {
        self.funcs.insert(String::from(name), func);
    }
}

pub struct SubScope {
    vars: HashMap<String, Types>,
    funcs: HashMap<String, Function>,
}

impl Scope for SubScope {
    fn is_global_scope(&self) -> bool {
        false
    }

    fn get_variable(&self, name: String) -> Option<Types> {
        let ret = self.vars.get(&name);
        if ret.is_some() {
            return Some(ret.unwrap().clone());
        }
        None
    }

    fn set_variable(&mut self, name: String, v: Types) -> Result<bool, String> {
        self.vars.insert(name, v);
        Ok(true)
    }

    fn get_func(&self, name: String) -> Option<&Function> {
        self.funcs.get(&name)
    }

    fn has_func(&self, name: String) -> bool {
        self.funcs.contains_key(&name)
    }

    fn put_func(&mut self, name: &str, func: Function) {
        self.funcs.insert(String::from(name), func);
    }
}
