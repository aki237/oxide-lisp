use crate::oxl::ast::*;
use crate::oxl::vm::*;
use crate::oxl::value::*;
use crate::oxl::token::*;

#[derive(Debug)]
pub struct SetCall {
    name: String,
    value: HeapEval,
    // File Bounds
}

impl SetCall {
    pub fn eval(&self, scope: &mut dyn Scope) -> Result<Types, String> {
        let result = self.value.eval(scope);
        if result.is_ok() {
            let res = scope.set_variable(String::from(&self.name), result.ok().unwrap());
            match res {
                Ok(ok) => {
                    return Ok(Types::Bool(ok));
                }
                Err(err) => {
                    return Err(err);
                }
            }
        }

        return Err(result.err().unwrap());
    }

    pub fn from(call: Token, args: Vec<ArgumentTypes>) -> Evalable {
        if args.len() != 2 {
            panic!(
                "set block should have max of 2 arguments. for the variable name and another for the actual value to be set. got {}, {}:{}",
                args.len(),
                call.line,
                call.col,
            );
        }
        let mut args_iter = args.into_iter();
        match (args_iter.next().unwrap(), args_iter.next().unwrap()) {
            (ArgumentTypes::Evalable(Evalable::VARIABLE(x)), ArgumentTypes::Evalable(y)) => {
                Evalable::SUBEXP(SubExpression::Set(SetCall::new(x, Box::new(y))))
            }
            (any, _) => panic!(
                "expected a variable name for the set call, got {:?}, {}:{}",
                any, call.line, call.col
            ),
        }
    }

    pub fn new(varname: String, valeval: HeapEval) -> SetCall {
        SetCall {
            name: varname,
            value: valeval,
        }
    }
}