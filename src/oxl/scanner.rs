// use std::vec::Vec;
use crate::oxl::token::*;
use std::fs::File;
use std::io::prelude::*;

pub struct Scanner {
    filename: String,
}

impl Scanner {
    pub fn init(filename: String) -> Scanner {
        Scanner { filename: filename }
    }

    pub fn get_tokens(&self) -> Result<Vec<Token>, String> {
        let file = File::open(&self.filename).unwrap();
        let mut tokens = std::vec::Vec::new();
        let mut tmp = String::new();
        let mut in_string = false;
        let mut in_comments = false;

        let mut pos = 0;
        let mut line = 1;
        let mut col = 0;

        for i in file.bytes() {
            let is_delim:bool;
            pos += 1;
            col += 1;

            let x:u8;
            match i {
                Ok(any) => x = any,
                Err(err) => {
                    return Err(err.to_string());
                }
            }
            // Collect comments.
            if in_comments {
                if x != 10 {
                    tmp += std::str::from_utf8(&vec![x]).unwrap();
                    continue;
                }
            }

            match x {
                59 => {
                    if in_string {
                        tmp += ";";
                        break;
                    }
                    if tmp.len() > 0 {
                        tokens.push(Token {
                            position: pos - 1 - (tmp.len() as u64),
                            col: col,
                            line: line,
                            token_type: TokenType::get_token_type(&tmp),
                        });
                        tmp = String::new();
                    }
                    tmp += ";";
                    in_comments = true;
                    continue;
                },
                40 => {
                    if in_string {
                        tmp += "(";
                        break;
                    }

                    if tmp.len() > 0 {
                        tokens.push(Token {
                            position: pos - 1 - (tmp.len() as u64),
                            col: col,
                            line: line,
                            token_type: TokenType::get_token_type(&tmp),
                        });
                        tmp = String::new();
                    }

                    tokens.push(Token {
                        position: pos - 1,
                        col: col,
                        line: line,
                        token_type: TokenType::LPAREN,
                    });
                    continue;
                }
                41 => {
                    if in_string {
                        tmp += ")";
                        break;
                    }
                    if tmp.len() > 0 {
                        tokens.push(Token {
                            position: pos - 1 - (tmp.len() as u64),
                            col: col,
                            line: line,
                            token_type: TokenType::get_token_type(&tmp),
                        });
                        tmp = String::new();
                    }
                    tokens.push(Token {
                        position: pos - 1,
                        col: col,
                        line: line,
                        token_type: TokenType::RPAREN,
                    });
                    continue;
                }
                10 => {
                    line = line + 1;
                    col = 0;
                    is_delim = true;
                    in_comments = false;
                    if in_string {
                        return Err(format!(
                            "string literals cannot contain new lines ({}, {})",
                            line, col
                        ));
                    }
                }
                32 => {
                    if in_string {
                        tmp += " ";
                        continue;
                    }
                    is_delim = true;
                }
                9 => {
                    if in_string {
                        tmp += "\t";
                        continue;
                    }
                    is_delim = true;
                }
                34 => {
                    if !in_string && tmp.len() != 0 {
                        return Err(format!(
                            "no separator found between list arguments ({}, {})",
                            line, col
                        ));
                    }
                    in_string = !in_string;
                    tmp += "\"";
                    continue;
                }
                any => {
                    tmp += std::str::from_utf8(&vec![any]).unwrap();
                    continue;
                }
            }
            if tmp.len() > 0 {
                tokens.push(Token {
                    token_type: TokenType::get_token_type(&tmp),
                    position: pos - 1 - (tmp.len() as u64),
                    line: line,
                    col: col,
                });
                if is_delim {
                    tmp = String::new();
                }
            }
        }

        if tmp.len() > 0 {
            return Err(format!(
                "EOF while scanning tokens: at ({}:{}) '{}'",
                line,
                col - tmp.len() as u64,
                tmp
            ));
        }

        Ok(tokens)
    }
}
