//use crate::oxl::token;
use crate::oxl::token::*;
use crate::oxl::value::*;
use crate::oxl::vm::*;

use crate::oxl::inbuilt_switch::*;
use crate::oxl::inbuilt_set::*;
use crate::oxl::inbuilt_if::*;
use crate::oxl::inbuilt_fn_call::*;


#[derive(Debug)]
pub enum SubExpression {
    /// Pass function/scope context to all evaluations
    // SWITCH + CASE Block
    // WHILE + BREAK + CONTINUE (Pass loop context)
    // FN Block
    // DEFINE
    // LET
    // EVAL ??
    Switch(SwitchBlock),
    Set(SetCall),
    If(IfBlock),
    FnCall(Call),
}

impl SubExpression {
    fn eval(&self, scope: &mut dyn Scope) -> Result<Types, String> {
        match self {
            SubExpression::If(i) => i.eval(scope),
            SubExpression::FnCall(c) => c.eval(scope),
            SubExpression::Set(s) => s.eval(scope),
            SubExpression::Switch(s) => s.eval(scope),
            //_ => Err(String::from("SubExpression NO_IMPL")),
        }
    }

    pub fn from(call: Token, args: Vec<ArgumentTypes>) -> Evalable {
        match &call.token_type {
            TokenType::KEYWORD(Keywords::IF) => IfBlock::from(call, args),
            TokenType::KEYWORD(Keywords::SET) => SetCall::from(call, args),
            TokenType::KEYWORD(Keywords::SWITCH) => SwitchBlock::from(call, args),
            TokenType::IDENTIFIER(x) => Call::from(Token{
                position: call.position,
                line: call.line,
                col: call.col,
                token_type: TokenType::IDENTIFIER(String::from(x)),
            }, String::from(x), args),
            _ => panic!("NOT POSSIBLE")
        }
    }
}

#[derive(Debug)]
pub enum Evalable {
    SUBEXP(SubExpression),
    VARIABLE(String),
    LITERAL(Types),
    NONE,
}

impl Evalable {
    pub fn eval(&self, scope: &mut dyn Scope) -> Result<Types, String> {
        match self {
            Evalable::SUBEXP(sx) => sx.eval(scope),
            Evalable::LITERAL(l) => Ok(l.clone()),
            Evalable::VARIABLE(varname) => {
                let var_opt = scope.get_variable(String::from(varname));
                match var_opt {
                    Some(v) => Ok(v),
                    None => Ok(Types::None),
                }
            }
            Evalable::NONE => Ok(Types::None),
        }
    }

    pub fn argtvec_to_evalables(args: Vec<ArgumentTypes>, line: u64, col: u64) -> Vec<Evalable> {
        args.into_iter()
            .map(|a| -> Evalable {
                match a {
                    ArgumentTypes::Evalable(x) => x,
                    any => panic!("expected an evalable, got {:?} {}:{}", any, line, col),
                }
            })
            .collect()
    }

    pub fn from(call: Token, args: Vec<ArgumentTypes>) -> Evalable {
        match call.token_type {
            TokenType::IDENTIFIER(_)
            | TokenType::KEYWORD(Keywords::IF)
            | TokenType::KEYWORD(Keywords::SET)
            | TokenType::KEYWORD(Keywords::SWITCH) => SubExpression::from(call, args),
            TokenType::KEYWORD(any) => panic!("{} not implemented {}:{}", any, call.line, call.col),
            _ => Evalable::NONE,
        }
    }
}

pub type HeapEval = Box<Evalable>;