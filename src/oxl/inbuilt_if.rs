use crate::oxl::ast::*;
use crate::oxl::vm::*;
use crate::oxl::value::*;
use crate::oxl::token::*;

#[derive(Debug)]
pub struct IfBlock {
    condition: HeapEval,
    success: HeapEval,
    fail: Option<HeapEval>,
    // File Bounds
}

impl IfBlock {
    pub fn eval(&self, scope: &mut dyn Scope) -> Result<Types, String> {
        let result = self.condition.eval(scope);
        if result.is_err() {
            return result;
        }

        let res_as_bool = result.unwrap().as_bool();
        if res_as_bool.is_none() {
            return Err(String::from(format!(
                "if condition evaluated to a none type"
            )));
        }

        if res_as_bool.unwrap() {
            return self.success.eval(scope);
        }

        if self.fail.is_some() {
            return self.fail.as_ref().unwrap().eval(scope);
        }

        Ok(Types::None)
    }

    pub fn from(call: Token, args: Vec<ArgumentTypes>) -> Evalable {
        match args.len() {
            2 => {
                let mut args_iter = Evalable::argtvec_to_evalables(args, call.line, call.col).into_iter();
                Evalable::SUBEXP(SubExpression::If(IfBlock::without_else(
                    Box::new(args_iter.nth(0).unwrap()),
                    Box::new(args_iter.nth(0).unwrap()),
                )))
            }
            3 => {
                let mut args_iter = Evalable::argtvec_to_evalables(args, call.line, call.col).into_iter();
                Evalable::SUBEXP(SubExpression::If(IfBlock::new(
                    Box::new(args_iter.nth(0).unwrap()),
                    Box::new(args_iter.nth(0).unwrap()),
                    Box::new(args_iter.nth(0).unwrap()),
                )))
            }
            _ => panic!(
                "if block should have minimum of 2 constructs for a condition and a success and a max of 2 (including an else clause),got {}, {}:{}",
                args.len(),
                call.line,
                call.col
            ),
        }
    }

    pub fn new(condition: HeapEval, success: HeapEval, fail: HeapEval) -> IfBlock {
        IfBlock {
            condition,
            success,
            fail: Some(fail),
        }
    }

    pub fn without_else(condition: HeapEval, success: HeapEval) -> IfBlock {
        IfBlock {
            condition,
            success,
            fail: None,
        }
    }
}