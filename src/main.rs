#[macro_use]
extern crate lazy_static;
mod functions;
mod oxl;
use crate::oxl::vm::*;

fn main() {
    let mut scope = oxl::vm::GlobalScope::new();
    scope.put_func("+", functions::plus);
    scope.put_func("*", functions::prod);
    scope.put_func(">", functions::gt);
    scope.put_func("=", functions::eq);
    scope.put_func("%", functions::modulo);
    scope.put_func("and", functions::and);
    scope.put_func("or", functions::or);
    scope.put_func("not", functions::not);
    scope.put_func("println", functions::println);

    scope.eval("test/arith.oxl")
}
